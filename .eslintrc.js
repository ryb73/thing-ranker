"use strict";

/** @type {import('@typescript-eslint/utils').TSESLint.Linter.Config} */
module.exports = {
  extends: [`plugin:@next/next/recommended`, `@ryb73`],

  rules: {
    "@typescript-eslint/lines-between-class-members": `off`,

    "import/no-unused-modules": [
      `warn`,
      {
        unusedExports: true,
        ignoreExports: [
          `src/app/**/head.tsx`,
          `src/app/**/layout.tsx`,
          `src/app/**/page.tsx`,
          `src/pages/api/*.ts`,
        ],
      },
    ],

    "jsx-a11y/label-has-associated-control": `off`,

    "react/jsx-handler-names": `off`,
  },
};
