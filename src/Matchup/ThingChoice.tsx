import styled from "@emotion/styled";
import { safeStyledElement } from "@ryb73/super-duper-parakeet/lib/src/safeStyled";
import { ExternalLink } from "@ryb73/super-duper-parakeet/lib/src/ui/ExternalLink";
import type { MouseEvent as ReactMouseEvent } from "react";
import { BiLinkExternal } from "react-icons/bi";
import { Button, buttonStyles } from "../common/Button";
import type { Thing } from "../types/Thing";

const ProductPageLink = styled(ExternalLink)(
  { display: `inline-block` },
  buttonStyles,
);

const H1 = styled.h1({
  fontSize: `1.5em`,
  lineHeight: 1,
  marginBottom: `0.5em`,
});

const H2 = styled.h1({
  fontSize: `1.2em`,
});

const ButtonRow = safeStyledElement(`div`, { isTop: true })(
  {
    display: `flex`,
    flexWrap: `wrap`,
    gap: 8,
    marginTop: 16,
  },
  ({ isTop }) => ({
    justifyContent: isTop ? `flex-start` : `flex-end`,
  }),
);

type Props = {
  readonly thing: Thing;
  readonly isTop: boolean;
  readonly onExclude: (event: ReactMouseEvent) => void;
  readonly onSelect: (event: ReactMouseEvent) => void;
};

const Container = safeStyledElement(`div`, { isTop: true })(
  {
    display: `flex`,
    justifyContent: `space-between`,
    gap: 16,
  },
  ({ isTop }) => ({
    flexDirection: isTop ? `row` : `row-reverse`,
    textAlign: isTop ? `left` : `right`,
  }),
);

const TextContainer = styled.div({
  padding: `2vw 0`,
});

const imageStyles = {
  alignSelf: `center`,
  flexBasis: `40%`,
  maxWidth: 125,
};

export function ThingChoice({ thing, isTop, onExclude, onSelect }: Props) {
  return (
    <Container isTop={isTop}>
      {/* eslint-disable-next-line @next/next/no-img-element */}
      <img alt="" src={thing.imageUrl} style={imageStyles} />
      <TextContainer>
        <H1>{thing.title}</H1>
        <H2>{thing.subtitle}</H2>
        <ButtonRow isTop={isTop}>
          <ProductPageLink href={thing.externalLink}>
            {thing.externalLinkTitle} <BiLinkExternal />
          </ProductPageLink>
          <Button onClick={onSelect} type="button">
            Select
          </Button>
          <Button onClick={onExclude} type="button">
            Remove From List
          </Button>
        </ButtonRow>
      </TextContainer>
    </Container>
  );
}
