import styled from "@emotion/styled";
import type { StyleableProps } from "@ryb73/super-duper-parakeet/lib/src/StylableProps";
import { defined } from "@ryb73/super-duper-parakeet/lib/src/type-checks";
import { useCallback } from "react";
import { Button } from "../common/Button";
import type { Thing } from "../types/Thing";
import { ThingChoice } from "./ThingChoice";

const MatchupContainer = styled.div({
  alignItems: `stretch`,
  display: `flex`,
  flexDirection: `column`,
  gap: `2vh`,
  height: `100%`,
  justifyContent: `center`,
});

export type Victory = {
  loserId: string;
  type: `victory`;
  winnerId: string;
};

// eslint-disable-next-line import/no-unused-modules
export type Skip = {
  type: `skip`;
};

export type ExcludeThing = {
  type: `excludeThing`;
  thingId: string;
};

export type MatchResult = ExcludeThing | Skip | Victory;

type Props = StyleableProps & {
  readonly getThingById: (id: string) => Thing | undefined;
  readonly matchup: [string, string];
  readonly onResolve: (result: MatchResult) => void;
};

function SkipButton({ onClick }: { readonly onClick: () => void }) {
  const handleClick = useCallback(() => onClick(), [onClick]);

  return (
    <Button onClick={handleClick} type="button">
      Skip
    </Button>
  );
}

export function Matchup({
  className,
  getThingById,
  matchup,
  onResolve,
  style,
}: Props) {
  const thing1 = defined(
    getThingById(matchup[0]),
    `invalid index: ${matchup[0]}`,
  );
  const thing2 = defined(
    getThingById(matchup[1]),
    `invalid index: ${matchup[1]}`,
  );

  const handleSkip = useCallback(
    () => onResolve({ type: `skip` }),
    [onResolve],
  );

  const handleSelect = useCallback(
    (which: `one` | `two`) => {
      const winnerId = which === `one` ? matchup[0] : matchup[1];
      const loserId = which === `one` ? matchup[1] : matchup[0];
      onResolve({ type: `victory`, winnerId, loserId });
    },
    [matchup, onResolve],
  );

  const handleExclude = useCallback(
    (which: `one` | `two`) => {
      const thingId = which === `one` ? matchup[0] : matchup[1];
      onResolve({ type: `excludeThing`, thingId });
    },
    [matchup, onResolve],
  );

  const selectThing1 = useCallback(() => handleSelect(`one`), [handleSelect]);
  const selectThing2 = useCallback(() => handleSelect(`two`), [handleSelect]);

  const excludeThing1 = useCallback(
    () => handleExclude(`one`),
    [handleExclude],
  );
  const excludeThing2 = useCallback(
    () => handleExclude(`two`),
    [handleExclude],
  );

  return (
    <MatchupContainer className={className} style={style}>
      <ThingChoice
        isTop
        onExclude={excludeThing1}
        onSelect={selectThing1}
        thing={thing1}
      />
      <ThingChoice
        isTop={false}
        onExclude={excludeThing2}
        onSelect={selectThing2}
        thing={thing2}
      />
      <SkipButton onClick={handleSkip} />
    </MatchupContainer>
  );
}
