import { keyframes } from "@emotion/react";
import styled from "@emotion/styled";
import type { StyleableProps } from "@ryb73/super-duper-parakeet/lib/src/StylableProps";
import { defined } from "@ryb73/super-duper-parakeet/lib/src/type-checks";
import { BareList } from "@ryb73/super-duper-parakeet/lib/src/ui/emotion/BareList";
import { useCallback, useMemo } from "react";
import { Button } from "../common/Button";
import type { Thing, ThingId } from "../types/Thing";

type Props = StyleableProps & {
  readonly sortedElScores: [ThingId, number][];
  readonly getThingById: (id: ThingId) => Thing | undefined;
  readonly reshuffleElo: () => void;
};

const Container = styled(BareList)({
  margin: `5vh 0`,
  maxHeight: `100%`,
  overflow: `auto`,
});

const List = styled(BareList)({});

const highlightAnimation = keyframes({
  "0%": {
    backgroundColor: `var(--bg-color-inverted)`,
    color: `var(--text-color-inverted)`,
  },
  "100%": {
    backgroundColor: `var(--bg-color)`,
    color: `var(--text-color)`,
  },
});

const ListItem = styled.li({
  animation: `${highlightAnimation} 4s ease forwards`,
  fontSize: 13,
  marginBottom: `0.5em`,
});

export function EloList({
  className,
  getThingById,
  reshuffleElo,
  sortedElScores,
  style,
}: Props) {
  const handleReshuffle = useCallback(() => reshuffleElo(), [reshuffleElo]);

  return (
    <Container className={className} style={style}>
      <Button
        onClick={handleReshuffle}
        style={{ marginBottom: 16 }}
        type="button"
      >
        Reshuffle
      </Button>
      <List as="ol">
        {useMemo(
          () =>
            sortedElScores.map(([id, score], i) => {
              const thing = defined(getThingById(id));
              return (
                <ListItem key={id}>
                  {i + 1}. [{score}] {thing.title}
                </ListItem>
              );
            }),
          [getThingById, sortedElScores],
        )}
      </List>
    </Container>
  );
}
