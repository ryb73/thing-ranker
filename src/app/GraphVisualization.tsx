import styled from "@emotion/styled";
import { filterUndefined } from "@ryb73/super-duper-parakeet/lib/src/collections/filterUndefined";
import type { StyleableProps } from "@ryb73/super-duper-parakeet/lib/src/StylableProps";
import {
  defined,
  isDefined,
} from "@ryb73/super-duper-parakeet/lib/src/type-checks";
import cytoscape from "cytoscape";
import type { DagreLayoutOptions } from "cytoscape-dagre";
import dagre from "cytoscape-dagre";
import { useEffect, useMemo, useState } from "react";
import CytoscapeComponent from "react-cytoscapejs";
import type { Thing, ThingId } from "../types/Thing";

cytoscape.use(dagre);

type Props = StyleableProps & {
  readonly getThingById: (id: string) => Thing | undefined;
  readonly lossesById: Map<string, string[]>;
  readonly sortedElScores: [ThingId, number][];
};

const StyledCytoscape = styled(CytoscapeComponent)({
  background: `#fffe`,
  border: `1px solid black`,
});

const cytoStylesheet: cytoscape.Stylesheet[] = [
  {
    selector: `node`,
    style: {
      "background-fit": `cover`,
      "background-image": `data(imageUrl)`,
      height: 230,
      shape: `rectangle`,
      width: 150,
    },
  },
  {
    selector: `edge`,
    style: {
      "curve-style": `bezier`,
      "line-color": `#9dbaea`,
      "target-arrow-color": `#9dbaea`,
      "target-arrow-shape": `triangle`,
      width: 2,
    },
  },
];

const graphLayout: DagreLayoutOptions = {
  name: `dagre`,
};

function thingsToNodes(things: Thing[]): cytoscape.NodeDefinition[] {
  return things.map((thing) => ({
    data: {
      id: thing.id,
      imageUrl: thing.imageUrl,
    },
  }));
}

function winnerLoserPairsToElements(
  lossesById: Map<string, string[]>,
  top26Set: Set<string>,
  getThingById: (id: string) => Thing | undefined,
): cytoscape.ElementDefinition[] {
  const edges = Array.from(lossesById.entries(), ([loserId, winnerIds]) =>
    !top26Set.has(loserId)
      ? []
      : filterUndefined(
          winnerIds.map((winnerId) => {
            if (!top26Set.has(winnerId)) return undefined;

            return {
              data: {
                source: winnerId,
                target: loserId,
              },
            };
          }),
        ),
  ).flat();

  const nodes = thingsToNodes(
    Array.from(top26Set.values(), (id) => defined(getThingById(id))),
  );

  return [...nodes, ...edges];
}

export function GraphVisualization({
  className,
  getThingById,
  lossesById,
  sortedElScores,
  style,
}: Props) {
  const top26Set = useMemo(
    () => new Set(sortedElScores.slice(0, 26).map(([id]) => id)),
    [sortedElScores],
  );

  const elements = useMemo(
    () => winnerLoserPairsToElements(lossesById, top26Set, getThingById),
    [getThingById, lossesById, top26Set],
  );

  const [cy, setCy] = useState<cytoscape.Core>();

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    elements;

    if (!isDefined(cy)) return;

    const layout = cy.layout(graphLayout);
    try {
      layout.run();
    } catch (error) {
      if (error instanceof Error && error.message !== `renderer is null`) {
        throw error;
      }
    }
  }, [cy, elements]);

  return (
    <StyledCytoscape
      className={className}
      cy={setCy}
      elements={elements}
      style={style}
      stylesheet={cytoStylesheet}
    />
  );
}
