import { bookToThing } from "../stateManagement/bookToThing";
import books from "./books.json";

export const bookThings = books.map((book) => bookToThing(book));
