import { defined } from "@ryb73/super-duper-parakeet/lib/src/type-checks";
import { BstMap } from "../rbx/BstMap";
import { div } from "../rbx/div";
import type { ThingId } from "../types/Thing";

type Scores = BstMap<ThingId, number>;

export function getScore(scores: Scores, thingId: ThingId) {
  return scores.get(thingId) ?? 1000;
}

function getWinProbability({
  opponentScore,
  targetScore,
}: {
  opponentScore: number;
  targetScore: number;
}) {
  return defined(div(1, 1 + 10 ** ((opponentScore - targetScore) / 400)));
}

export function calculateNewScores({
  loserScore,
  winnerScore,
}: {
  winnerScore: number;
  loserScore: number;
}) {
  const winnerWinProbability = getWinProbability({
    opponentScore: loserScore,
    targetScore: winnerScore,
  });
  const loserWinProbability = getWinProbability({
    opponentScore: winnerScore,
    targetScore: loserScore,
  });

  const winnerNewScore = Math.floor(
    winnerScore + 32 * (1 - winnerWinProbability),
  );
  const loserNewScore = Math.floor(loserScore + 32 * (0 - loserWinProbability));
  return {
    loserNewScore,
    winnerNewScore,
  };
}

export function calculateInitialEloScores(
  lossesByIdEntries: Iterable<readonly [ThingId, ThingId[]]>,
  allThingIds: Iterable<ThingId>,
): Scores {
  const scores: Scores = BstMap.empty(([aId, aScore], [bId, bScore]) =>
    aScore === bScore ? aId.localeCompare(bId) : bScore - aScore,
  );

  for (const [loserId, winnerIds] of lossesByIdEntries) {
    const loserScore = getScore(scores, loserId);

    winnerIds.forEach((winnerId) => {
      const winnerScore = getScore(scores, winnerId);
      const { loserNewScore, winnerNewScore } = calculateNewScores({
        winnerScore,
        loserScore,
      });
      scores.set(winnerId, winnerNewScore);
      scores.set(loserId, loserNewScore);
    });
  }

  for (const thingId of allThingIds) {
    if (!scores.has(thingId)) scores.set(thingId, 1000);
  }

  return scores;
}
