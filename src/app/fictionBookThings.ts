import { bookToThing } from "../stateManagement/bookToThing";
import fictionBooks from "./fictionBooks.json";

export const fictionBookThings = fictionBooks.map((book) => bookToThing(book));
