"use client";

import styled from "@emotion/styled";
import { enableMapSet } from "immer";
import { shuffle } from "lodash-es";
import type { Reducer } from "react";
import { useCallback, useMemo, useReducer } from "react";
import { generateLossesByIdEntries } from "../../common/generateLossesByIdEntries";
import type { BstMap } from "../../rbx/BstMap";
import { bookToThing } from "../../stateManagement/bookToThing";
import type { Thing, ThingId } from "../../types/Thing";
import lossesByIdJson0 from "../bookLossesById-0.json";
import lossesByIdJson1 from "../bookLossesById-1.json";
import lossesByIdJson2 from "../bookLossesById-2.json";
import lossesByIdJson3 from "../bookLossesById-3.json";
import lossesByIdJson4 from "../bookLossesById-4.json";
import books from "../books.json";
import { calculateInitialEloScores } from "../calculateInitialEloScores";
import { EloList } from "../EloList";

enableMapSet();

const things = books.map((book) => bookToThing(book));
const thingsById = new Map<ThingId, Thing>(
  things.map((thing) => [thing.id, thing]),
);

const allThingIds = things.map((thing) => thing.id);

type State = {
  runs: {
    eloScoreByThingId: BstMap<string, number>;
    lossesById: Map<string, string[]>;
  }[];
};

type ReshuffleElo = {
  type: `reshuffleElo`;
};

type Action = ReshuffleElo;

function getThingById(id: string) {
  return thingsById.get(id);
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case `reshuffleElo`: {
      return {
        ...state,
        runs: state.runs.map((run) => ({
          ...run,
          eloScoreByThingId: calculateInitialEloScores(
            shuffle(Array.from(run.lossesById.entries())),
            allThingIds,
          ),
        })),
      };
    }
  }
  // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
  throw new Error(`Unknown action type: ${(action as Action).type}`);
}

const Main = styled.main({
  alignItems: `stretch`,
  display: `flex`,
  flexGrow: 1,
  gap: `5vw`,
  maxHeight: `100%`,
  padding: `2vw`,
});

const lossesByIdEntries0 = generateLossesByIdEntries(
  lossesByIdJson0,
  thingsById,
);
const lossesByIdEntries1 = generateLossesByIdEntries(
  lossesByIdJson1,
  thingsById,
);
const lossesByIdEntries2 = generateLossesByIdEntries(
  lossesByIdJson2,
  thingsById,
);
const lossesByIdEntries3 = generateLossesByIdEntries(
  lossesByIdJson3,
  thingsById,
);
const lossesByIdEntries4 = generateLossesByIdEntries(
  lossesByIdJson4,
  thingsById,
);

const StyledEloList = styled(EloList)({
  width: `30%`,
});

export default function Home() {
  const [state, dispatch] = useReducer<Reducer<State, Action>, null>(
    reducer,
    null,
    (): State => ({
      runs: [
        {
          eloScoreByThingId: calculateInitialEloScores(
            lossesByIdEntries0,
            allThingIds,
          ),
          lossesById: new Map(lossesByIdEntries0),
        },
        {
          eloScoreByThingId: calculateInitialEloScores(
            lossesByIdEntries1,
            allThingIds,
          ),
          lossesById: new Map(lossesByIdEntries1),
        },
        {
          eloScoreByThingId: calculateInitialEloScores(
            lossesByIdEntries2,
            allThingIds,
          ),
          lossesById: new Map(lossesByIdEntries2),
        },
        {
          eloScoreByThingId: calculateInitialEloScores(
            lossesByIdEntries3,
            allThingIds,
          ),
          lossesById: new Map(lossesByIdEntries3),
        },
        {
          eloScoreByThingId: calculateInitialEloScores(
            lossesByIdEntries4,
            allThingIds,
          ),
          lossesById: new Map(lossesByIdEntries4),
        },
      ],
    }),
  );

  const reshuffleElo = useCallback(
    () =>
      dispatch({
        type: `reshuffleElo`,
      }),
    [],
  );

  const sortedElScores0 = useMemo(
    () => Array.from(state.runs[0]!.eloScoreByThingId),
    [state.runs],
  );
  const sortedElScores1 = useMemo(
    () => Array.from(state.runs[1]!.eloScoreByThingId),
    [state.runs],
  );
  const sortedElScores2 = useMemo(
    () => Array.from(state.runs[2]!.eloScoreByThingId),
    [state.runs],
  );
  const sortedElScores3 = useMemo(
    () => Array.from(state.runs[3]!.eloScoreByThingId),
    [state.runs],
  );
  const sortedElScores4 = useMemo(
    () => Array.from(state.runs[4]!.eloScoreByThingId),
    [state.runs],
  );

  return (
    <Main>
      <StyledEloList
        getThingById={getThingById}
        reshuffleElo={reshuffleElo}
        sortedElScores={sortedElScores0}
      />
      <StyledEloList
        getThingById={getThingById}
        reshuffleElo={reshuffleElo}
        sortedElScores={sortedElScores1}
      />
      <StyledEloList
        getThingById={getThingById}
        reshuffleElo={reshuffleElo}
        sortedElScores={sortedElScores2}
      />
      <StyledEloList
        getThingById={getThingById}
        reshuffleElo={reshuffleElo}
        sortedElScores={sortedElScores3}
      />
      <StyledEloList
        getThingById={getThingById}
        reshuffleElo={reshuffleElo}
        sortedElScores={sortedElScores4}
      />
    </Main>
  );
}
