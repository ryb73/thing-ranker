import { isDefined } from "@ryb73/super-duper-parakeet/lib/src/type-checks";

export function addWin(
  winsById: Map<string, string[]>,
  winnerId: string,
  loserId: string,
) {
  const wins = winsById.get(winnerId);
  if (isDefined(wins)) {
    wins.push(loserId);
  } else {
    winsById.set(winnerId, [loserId]);
  }
}

export function addLoss(
  lossesById: Map<string, string[]>,
  loserId: string,
  winnerId: string,
) {
  const losses = lossesById.get(loserId);
  if (isDefined(losses)) {
    losses.push(winnerId);
  } else {
    lossesById.set(loserId, [winnerId]);
  }
}
