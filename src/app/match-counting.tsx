import type { Thing } from "../types/Thing";

export function incrementNumMatchesIndex(
  thingIdsByNumberOfMatches: Map<number, string[]>,
  numMatchesByThingId: Map<string, number>,
  thingId: string,
  incrementBy = 1,
) {
  const previouseNumMatches = numMatchesByThingId.get(thingId) ?? 0;
  thingIdsByNumberOfMatches.set(
    previouseNumMatches,
    thingIdsByNumberOfMatches
      .get(previouseNumMatches)
      ?.filter((id) => id !== thingId) ?? [],
  );

  const newNumMatches = previouseNumMatches + incrementBy;
  thingIdsByNumberOfMatches.set(newNumMatches, [
    ...(thingIdsByNumberOfMatches.get(newNumMatches) ?? []),
    thingId,
  ]);
}

export function calculateNumberOfMatches(
  things: Thing[],
  lossesById: Map<string, string[]>,
) {
  const numMatchesByThingId = new Map<string, number>();
  const thingIdsByNumberOfMatches = new Map<number, string[]>();

  things.forEach(({ id }) => {
    numMatchesByThingId.set(id, 0);
    thingIdsByNumberOfMatches.set(0, [
      ...(thingIdsByNumberOfMatches.get(0) ?? []),
      id,
    ]);
  });

  lossesById.forEach((winnerIds, loserId) => {
    const numMatches = winnerIds.length;
    incrementNumMatchesIndex(
      thingIdsByNumberOfMatches,
      numMatchesByThingId,
      loserId,
      numMatches,
    );
    numMatchesByThingId.set(
      loserId,
      (numMatchesByThingId.get(loserId) ?? 0) + numMatches,
    );

    winnerIds.forEach((winnerId) => {
      incrementNumMatchesIndex(
        thingIdsByNumberOfMatches,
        numMatchesByThingId,
        winnerId,
      );
      numMatchesByThingId.set(
        winnerId,
        (numMatchesByThingId.get(winnerId) ?? 0) + 1,
      );
    });
  });

  return { thingIdsByNumberOfMatches, numMatchesByThingId };
}
