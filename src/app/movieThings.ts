import { movieToThing } from "../stateManagement/movieToThing";
import reelgood from "./reelgood.json";

export const movieThings = reelgood
  .filter(({ format, isWatched }) => format === `movie` && !isWatched)
  .map((movie) => movieToThing(movie));
