"use client";

import styled from "@emotion/styled";
import { assert } from "@ryb73/super-duper-parakeet/lib/src/assert";
import { useWatchValue } from "@ryb73/super-duper-parakeet/lib/src/debugging/useWatchValue";
import {
  defined,
  isDefined,
} from "@ryb73/super-duper-parakeet/lib/src/type-checks";
import { enableMapSet, produce } from "immer";
import { shuffle } from "lodash-es";
import type { Reducer } from "react";
import { useCallback, useEffect, useMemo, useReducer } from "react";
import { generateLossesByIdEntries } from "../common/generateLossesByIdEntries";
import type { ExcludeThing, MatchResult, Victory } from "../Matchup";
import { Matchup } from "../Matchup";
import type { BstMap } from "../rbx/BstMap";
import type { Thing, ThingId } from "../types/Thing";
import lossesByIdJson from "./movieLossesById.json";
import { movieThings } from "./movieThings";
import {
  calculateInitialEloScores,
  calculateNewScores,
  getScore,
} from "./calculateInitialEloScores";
import { EloList } from "./EloList";
import { GraphVisualization } from "./GraphVisualization";
import { addLoss } from "./manipulate-match-result-maps";
import {
  calculateNumberOfMatches,
  incrementNumMatchesIndex,
} from "./match-counting";

enableMapSet();

const things = movieThings;

const thingsById = new Map<ThingId, Thing>(
  things.map((thing) => [thing.id, thing]),
);

type DefendingChamp = {
  id: ThingId;
  numMatches: number;
};

type State = {
  currentMatchup?: [ThingId, ThingId];
  defendingChamp?: DefendingChamp;
  eloScoreByThingId?: BstMap<ThingId, number>;
  lossesById: Map<ThingId, ThingId[]>;
  numMatchesByThingId: Map<ThingId, number>;
  sortedEloScores?: [ThingId, number][];
  thingIdsByNumberOfMatches: Map<number, ThingId[]>;
};

function getRandomChallengerIndex(
  sortedCandidates: ThingId[],
  numDefending?: number,
) {
  const exponent = 1 + ((numDefending ?? 1) - 1) * 0.8;
  const randomNumber = Math.random() ** exponent;

  return Math.floor(randomNumber * sortedCandidates.length);
}

function getNextMatchup(state: State, iterations = 0): [ThingId, ThingId] {
  assert(iterations < 3, `Too many iterations`);

  const sortedIds = defined(state.sortedEloScores).map(([id]) => id);

  const thingId1Index = isDefined(state.defendingChamp?.id)
    ? sortedIds.indexOf(state.defendingChamp!.id)
    : getRandomChallengerIndex(sortedIds);

  const thingId1 = defined(sortedIds[thingId1Index]);

  const lowestNumMatches = Array.from(
    state.thingIdsByNumberOfMatches.keys(),
  ).reduce(
    (acc, numMatches) => Math.min(acc, numMatches),
    Number.POSITIVE_INFINITY,
  );

  const thingsWithFewestMatches =
    state.thingIdsByNumberOfMatches.get(lowestNumMatches) ?? [];

  const beginIndex = Math.max(0, thingId1Index - 25);
  const thing2Candidates = [
    ...thingsWithFewestMatches,
    ...sortedIds.slice(beginIndex, beginIndex + 25),
  ];
  const thingId2Index = getRandomChallengerIndex(
    thing2Candidates,
    state.defendingChamp?.numMatches,
  );
  const thingId2 = defined(thing2Candidates[thingId2Index]);

  if (thingId1 === thingId2) {
    return getNextMatchup(state, iterations + 1);
  }

  return [thingId1, thingId2];
}

type ProcessMatchupResult = {
  type: `processMatchupResult`;
  matchResult: MatchResult;
};

type SetMatchup = {
  type: `setMatchup`;
  matchup: [string, string];
};

type ReshuffleElo = {
  type: `reshuffleElo`;
};

type SetInitialEloScores = {
  type: `setInitialEloScores`;
  eloScoreByThingId: BstMap<string, number>;
};

type Action =
  | ProcessMatchupResult
  | ReshuffleElo
  | SetInitialEloScores
  | SetMatchup;

function getThingById(id: string) {
  return thingsById.get(id);
}

function winnerIsDone(
  winnerId: string,
  sortedEloScores: [string, number][],
  defendingChamp?: DefendingChamp,
) {
  if (defendingChamp?.id === winnerId && defendingChamp.numMatches >= 4)
    return true;

  // Don't continue feeding challengers to anyone in the top 10
  return (
    sortedEloScores[0]![0] === winnerId ||
    sortedEloScores[1]![0] === winnerId ||
    sortedEloScores[2]![0] === winnerId ||
    sortedEloScores[3]![0] === winnerId ||
    sortedEloScores[4]![0] === winnerId ||
    sortedEloScores[5]![0] === winnerId ||
    sortedEloScores[6]![0] === winnerId ||
    sortedEloScores[7]![0] === winnerId ||
    sortedEloScores[8]![0] === winnerId ||
    sortedEloScores[9]![0] === winnerId
  );
}

function processVictory(state: State, { loserId, winnerId }: Victory) {
  return produce(state, (draft) => {
    const { thingIdsByNumberOfMatches, lossesById, numMatchesByThingId } =
      draft;
    const eloScoreByThingId = defined(draft.eloScoreByThingId);

    addLoss(lossesById, loserId, winnerId);

    incrementNumMatchesIndex(
      thingIdsByNumberOfMatches,
      numMatchesByThingId,
      loserId,
    );
    incrementNumMatchesIndex(
      thingIdsByNumberOfMatches,
      numMatchesByThingId,
      winnerId,
    );

    numMatchesByThingId.set(
      loserId,
      (numMatchesByThingId.get(loserId) ?? 0) + 1,
    );
    numMatchesByThingId.set(
      winnerId,
      (numMatchesByThingId.get(winnerId) ?? 0) + 1,
    );

    const { loserNewScore, winnerNewScore } = calculateNewScores({
      loserScore: getScore(
        eloScoreByThingId as BstMap<string, number>,
        loserId,
      ),
      winnerScore: getScore(
        eloScoreByThingId as BstMap<string, number>,
        winnerId,
      ),
    });

    draft.eloScoreByThingId = eloScoreByThingId
      .set(loserId, loserNewScore)
      .set(winnerId, winnerNewScore);

    draft.sortedEloScores = Array.from(draft.eloScoreByThingId);

    draft.defendingChamp = winnerIsDone(
      winnerId,
      draft.sortedEloScores,
      draft.defendingChamp,
    )
      ? undefined
      : {
          id: winnerId,
          numMatches:
            draft.defendingChamp?.id === winnerId
              ? draft.defendingChamp.numMatches + 1
              : 1,
        };
    draft.currentMatchup = undefined;
  });
}

function processSkip({
  defendingChamp,
  eloScoreByThingId,
  lossesById,
  numMatchesByThingId,
  sortedEloScores: sortedElScores,
  thingIdsByNumberOfMatches,
}: State): State {
  return {
    defendingChamp,
    eloScoreByThingId,
    lossesById,
    numMatchesByThingId,
    sortedEloScores: sortedElScores,
    thingIdsByNumberOfMatches,
  };
}

function processExcludeThing(state: State, { thingId }: ExcludeThing) {
  return produce(state, (draft) => {
    draft.currentMatchup = undefined;

    const numMatches = draft.numMatchesByThingId.get(thingId) ?? 0;
    draft.thingIdsByNumberOfMatches.set(
      numMatches,
      draft.thingIdsByNumberOfMatches
        .get(numMatches)
        ?.filter((id) => id !== thingId) ?? [],
    );
    draft.numMatchesByThingId.delete(thingId);
  });
}

function processMatchResult(action: ProcessMatchupResult, state: State): State {
  switch (action.matchResult.type) {
    case `victory`:
      return processVictory(state, action.matchResult);
    case `skip`: {
      return processSkip(state);
    }
    case `excludeThing`: {
      return processExcludeThing(state, action.matchResult);
    }
  }
  throw new Error(
    `Unknown match result type: ${(action.matchResult as MatchResult).type}`,
  );
}

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case `processMatchupResult`: {
      return processMatchResult(action, state);
    }
    case `setMatchup`: {
      return {
        ...state,
        currentMatchup: action.matchup,
      };
    }
    case `reshuffleElo`: {
      return {
        ...state,
        eloScoreByThingId: undefined,
        sortedEloScores: [],
      };
    }
    case `setInitialEloScores`: {
      return {
        ...state,
        eloScoreByThingId: action.eloScoreByThingId,
        sortedEloScores: Array.from(action.eloScoreByThingId),
      };
    }
  }
  throw new Error(`Unknown action type: ${(action as Action).type}`);
}

const Main = styled.main({
  alignItems: `stretch`,
  display: `flex`,
  flexGrow: 1,
  gap: `5vw`,
  maxHeight: `100%`,
  padding: `2vw`,
});

const lossesByIdEntries = generateLossesByIdEntries(lossesByIdJson, thingsById);

const StyledMatchup = styled(Matchup)({
  width: `30%`,
});

const MatchupPlaceholder = styled.div({
  flexShrink: 0,
  width: `30%`,
});

const StyledGraphVisualization = styled(GraphVisualization)({
  width: `40%`,
});

const StyledEloList = styled(EloList)({
  width: `30%`,
});

function getInitialState(): State {
  const lossesByIdMap = new Map(lossesByIdEntries);
  const { thingIdsByNumberOfMatches, numMatchesByThingId } =
    calculateNumberOfMatches(things, lossesByIdMap);

  return {
    lossesById: lossesByIdMap,
    numMatchesByThingId,
    thingIdsByNumberOfMatches,
  };
}

export default function Home() {
  const [state, dispatch] = useReducer<Reducer<State, Action>, null>(
    reducer,
    null,
    getInitialState,
  );

  useWatchValue(
    `losses`,
    useMemo(
      () => Object.fromEntries(state.lossesById.entries()),
      [state.lossesById],
    ),
  );

  useWatchValue(`thingIdsByNumberOfMatches`, state.thingIdsByNumberOfMatches);

  useWatchValue(
    `numMatches`,
    useMemo(
      () =>
        Array.from(state.thingIdsByNumberOfMatches.entries()).reduce(
          (acc, [numMatches, thingIds]) => acc + numMatches * thingIds.length,
          0,
        ) / 2,
      [state.thingIdsByNumberOfMatches],
    ),
  );

  const handleResolve = useCallback(
    (matchResult: MatchResult) =>
      dispatch({
        type: `processMatchupResult`,
        matchResult,
      }),
    [],
  );

  useEffect(() => {
    if (!isDefined(state.eloScoreByThingId)) {
      dispatch({
        type: `setInitialEloScores`,
        eloScoreByThingId: calculateInitialEloScores(
          shuffle(Array.from(state.lossesById.entries())),
          state.numMatchesByThingId.keys(),
        ),
      });
    }
  }, [state.eloScoreByThingId, state.lossesById, state.numMatchesByThingId]);

  useEffect(() => {
    if (
      !isDefined(state.currentMatchup) &&
      isDefined(state.eloScoreByThingId)
    ) {
      dispatch({
        type: `setMatchup`,
        matchup: getNextMatchup(state),
      });
    }
  }, [state]);

  const reshuffleElo = useCallback(
    () =>
      dispatch({
        type: `reshuffleElo`,
      }),
    [],
  );

  if (!isDefined(state.eloScoreByThingId)) return null;

  return (
    <Main>
      {isDefined(state.currentMatchup) ? (
        <StyledMatchup
          getThingById={getThingById}
          matchup={state.currentMatchup}
          onResolve={handleResolve}
        />
      ) : (
        <MatchupPlaceholder />
      )}
      <StyledGraphVisualization
        getThingById={getThingById}
        lossesById={state.lossesById}
        // eslint-disable-next-line react-perf/jsx-no-new-array-as-prop
        sortedElScores={state.sortedEloScores ?? []}
      />
      <StyledEloList
        getThingById={getThingById}
        reshuffleElo={reshuffleElo}
        // eslint-disable-next-line react-perf/jsx-no-new-array-as-prop
        sortedElScores={state.sortedEloScores ?? []}
      />
    </Main>
  );
}
