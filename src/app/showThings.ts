import { showToThing } from "../stateManagement/showToThing";
import shows from "./shows.json";

export const showThings = shows
  .filter(({ isWatched }) => !isWatched)
  .map((show) => showToThing(show));
