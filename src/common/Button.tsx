import type { Interpolation } from "@emotion/styled";
import styled from "@emotion/styled";
import { SafeButton } from "@ryb73/super-duper-parakeet/lib/src/ui/SafeButton";

// eslint-disable-next-line @typescript-eslint/ban-types
export const buttonStyles: Interpolation<{}> = {
  background: `var(--button-bg)`,
  border: 0,
  color: `var(--button-fg)`,
  cursor: `pointer`,
  fontSize: `1em`,
  fontWeight: `bold`,
  lineHeight: `inherit`,
  padding: `0.5em`,
};

export const Button = styled(SafeButton)(buttonStyles);
