import type { LossesByIdJson } from "../types/LossesByIdJson";
import type { Thing } from "../types/Thing";

export function generateLossesByIdEntries(
  lossesByIdJson: LossesByIdJson,
  thingsById: Map<string, Thing>,
) {
  return Object.entries(lossesByIdJson)
    .filter(([loserId]) => thingsById.has(loserId))
    .map(
      ([loserId, winners]) =>
        [
          loserId,
          winners.filter((winnerId) => thingsById.has(winnerId)),
        ] as const,
    );
}
