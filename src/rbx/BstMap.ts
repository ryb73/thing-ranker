import {
  defined,
  isDefined,
} from "@ryb73/super-duper-parakeet/lib/src/type-checks";
import { RBTree } from "bintrees";

export class BstMap<Key, Value> implements Iterable<[Key, Value]> {
  public static empty<Key, Value>(
    compare: (a: [Key, Value], b: [Key, Value]) => number,
  ): BstMap<Key, Value> {
    return new BstMap<Key, Value>(compare);
  }

  private readonly map = new Map<Key, Value>();
  private readonly bst: RBTree<[Key, Value]>;

  // These commented lines are throwing an ESLint error
  // private constructor(compare: (a: [Key, Value], b: [Key, Value]) => number);
  // private constructor(map: Map<Key, Value>, bst: RBTree<[Key, Value]>);
  private constructor(
    mapOrCompare:
      | Map<Key, Value>
      | ((a: [Key, Value], b: [Key, Value]) => number),
    bst?: RBTree<[Key, Value]>,
  ) {
    if (mapOrCompare instanceof Map) {
      this.map = mapOrCompare;
      this.bst = defined(bst);
    } else {
      this.bst = new RBTree<[Key, Value]>(mapOrCompare);
    }
  }

  public has(key: Key): boolean {
    return this.map.has(key);
  }

  public get(key: Key): Value | undefined {
    return this.map.get(key);
  }

  /**
   * Mutates the BstMap and returns a new instance (to force reference inequality)
   */
  public set(key: Key, value: Value): BstMap<Key, Value> {
    const existingValue = this.map.get(key);

    if (isDefined(existingValue)) this.bst.remove([key, existingValue]);

    this.map.set(key, value);
    this.bst.insert([key, value]);

    return new BstMap(this.map, this.bst);
  }

  public [Symbol.iterator](): Iterator<[Key, Value]> {
    const bstIterator = this.bst.iterator();

    return {
      next: () => {
        const next = bstIterator.next();

        if (!isDefined(next)) return { done: true, value: undefined };

        return {
          done: false,
          value: next,
        };
      },
    };
  }
}
