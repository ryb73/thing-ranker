export function div(numerator: number, denominator: number) {
  try {
    // eslint-disable-next-line total-functions/no-partial-division
    return numerator / denominator;
  } catch {
    return undefined;
  }
}
