import type books from "../app/books.json";
import type { Thing } from "../types/Thing";

function generateAmazonProductUrl(amazonId: string) {
  return `https://www.amazon.com/gp/product/${amazonId}`;
}

export function bookToThing(book: (typeof books)[number]): Thing {
  return {
    externalLink: generateAmazonProductUrl(book.amazonId),
    externalLinkTitle: `Product Page`,
    id: book.amazonId,
    imageUrl: book.imageUrl,
    subtitle: book.authorsString,
    title: book.name,
  };
}
