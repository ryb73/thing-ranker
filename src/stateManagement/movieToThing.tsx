import type movies from "../app/movies.json";
import type { Thing } from "../types/Thing";

export function movieToThing(movie: (typeof movies)[number]): Thing {
  return {
    externalLink: movie.url,
    externalLinkTitle: `Reelgood URL`,
    id: movie.url,
    imageUrl: movie.imageUrl ?? `https://singlecolorimage.com/get/eef0f8/1x1`,
    title: movie.name,
  };
}
