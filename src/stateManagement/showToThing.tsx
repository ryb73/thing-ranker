import type shows from "../app/shows.json";
import type { Thing } from "../types/Thing";

export function showToThing(show: (typeof shows)[number]): Thing {
  return {
    externalLink: show.url,
    externalLinkTitle: `Reelgood URL`,
    id: show.url,
    imageUrl: show.imageUrl,
    title: show.name,
  };
}
