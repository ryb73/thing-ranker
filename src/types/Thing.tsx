import type { TypeOf } from "io-ts";
import { exact, intersection, partial, strict, string } from "io-ts";

const ThingId = string;
type ThingId = TypeOf<typeof ThingId>;
export { ThingId };

const Thing = intersection([
  strict({
    externalLink: string,
    externalLinkTitle: string,
    id: ThingId,
    imageUrl: string,
    title: string,
  }),
  exact(
    partial({
      subtitle: string,
    }),
  ),
]);
type Thing = TypeOf<typeof Thing>;
export { Thing };
